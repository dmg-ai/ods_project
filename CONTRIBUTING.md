# Contributing to ODS project

Thank you for your interest in contributing to ODS project! This guide is designed to make it easier for you to get involved and help build a cool project.

For small changes (e.g., bug fixes), feel free to submit a PR.

For larger changes, consider
creating an [**issue**](https://gitlab.com/dmg-ai/ods_project/-/issues) outlining your proposed change.

## Getting Started

### Setting Up Your Development Environment

#### Fork and clone the repository

Start by forking the project repository to your GitHub account, then clone your fork to your local machine:

```bash
git clone https://gitlab.com/dmg-ai/ods_project
cd ods_project
```

#### Create a virtual environment

We recommend using a virtual environment to isolate project dependencies. Ensure you have Python 3.8 or higher installed on your machine, as it is the minimum supported version for a project. To create and activate a virtual environment, run the following commands:

#### Linux / macOS

```bash
python3 -m venv ods_venv
source ods_venv/bin/activate
```

#### Windows cmd.exe

```bash
python -m venv ods_venv
ods_venv\Scripts\activate.bat
```

#### Windows PowerShell

```bash
python -m venv ods_venv
ods_venv\Scripts\activate.ps1
```


## Navigating the Project

* The main source code is located in the `src/` directory.
* Tests are located in the `tests/` directory. Every pull request should include tests for new features or bug fixes.

## How to Contribute

### Types of Contributions

* **Code Contributions**: Whether it's fixing a bug, adding a new feature, or improving performance, your code contributions are valuable.
* **Documentation:** Help us improve the project's documentation for better usability and accessibility.
* **Bug Reports and Feature Requests**: Use GitHub Issues to report bugs, request features, or suggest improvements.

### Contribution Process

1. **Find an issue to work on**: Look for open issues or propose a new one. For newcomers, look for issues labeled "good first issue."
2. **Fork the repository** (if you haven't already).
3. **Create a new branch for your changes**: `git checkout -b feature/my-new-feature`.
4. **Implement your changes**: Write clean, readable, and well-documented code.
5. **Add or update tests** as necessary.
6. **Ensure all tests pass** and your code adheres to the existing style guidelines.
7. **Submit a Pull Request (PR):** Open a PR from your forked repository to the main ODS project repository. Provide a clear description of the changes and any relevant issue numbers.

### Code Review Process

* Once you submit a PR, the ODS project maintainers will review your contribution.
* Engage in the review process if the maintainers have feedback or questions.
* Once your PR is approved, a maintainer will merge it into the main codebase.

## Coding Guidelines

### Using linters and formatters

The following linters and formatters are used in the project:
* ruff (https://github.com/astral-sh/ruff)
* mypy (https://github.com/python/mypy)
* some pre-hooks checkers from https://github.com/pre-commit/pre-commit-hooks

Ruff is a project main linter and code formatter, written in Rust.

Usage:

1. Install `Ruff`: `pip install ruff`. Complete the command in the terminal: `ruff check`.
2. Ruff can also be used as a pre-commit hook. See `.pre-commit-config.yaml` for details.
3. Ruff can also be used as a VS Code extension ([vscode instructions](https://github.com/astral-sh/ruff-vscode)).

### Using Pre-commit Hooks

To maintain code quality and consistency, we use pre-commit hooks. Follow these steps to set up pre-commit hooks in your local repository:

**Install pre-commit:** If you haven't already, you need to install pre-commit on your machine. You can do this using pip:

```bash
pip install pre-commit
```

**Initialize pre-commit:**

Navigate to the root of your cloned repository and run:

```bash
pre-commit install
```

This command sets up the pre-commit hooks based on the configurations found in `.pre-commit-config.yaml` at the root of the repository.

**Running pre-commit hooks:**

Pre-commit will automatically run the configured hooks on each commit. You can also manually run the hooks on all files in the repository with:

```bash
pre-commit run --all-files
```

Ensure to fix any issues detected by the pre-commit hooks before submitting your pull request.

**Note:** to avoid import errors when executing mypy-pre-hook, run the following commands:
```
set -a
source  .env
```

### Running Tests

Before submitting your contributions, it's important to ensure that all tests pass. This helps maintain the stability and reliability of Albumentations. Here's how you can run the tests:

Install `pytest`:

```bash
pip install pytest
```

Run the tests:

With `pytest` installed, you can run all tests using the following command from the root of the repository:

```bash
pytest tests/test.py
```

This will execute all the tests and display the results, indicating whether each test passed or failed.

## Acknowledgements

Your contributions are appreciated and recognized. Contributors who have significantly impacted the project will be mentioned in our documentation and releases.

## Contact Information

For any questions or concerns about contributing, please reach out to the maintainers via email.
