def greet_the_guest(name: str = "Guest") -> str:
    return f"Hello, {name}!"


if __name__ == "__main__":
    print(greet_the_guest())
