# ODS project

## Homework 1

### Environment settings

To setup environment use [**Poetry**](https://python-poetry.org/) package manager.

Run the following commands to use `Poetry`:
```
pip install poetry
poetry init # run in your working directory (ods_project)
poetry install
poetry update # optional
```

All dependecies are list in `pyproject.toml`.

You can find `pre-commit-hooks` setup info at [CONTRIBUTING.md](CONTRIBUTING.md).

### Contributing

To create a pull request to the repository, follow the documentation at [CONTRIBUTING.md](CONTRIBUTING.md).
