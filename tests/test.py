from src.app import greet_the_guest


def test_dummy(name: str = "Guest") -> None:
    assert f"Hello, {name}!" == greet_the_guest(name=name)
